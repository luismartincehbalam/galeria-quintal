export const environment = {
  production: true,
  mapboxKey: 'pk.eyJ1IjoicmF6aWVsMjAxOSIsImEiOiJjazFxbTM1MnMwMWowM2xsYWs5eXZpNHNvIn0.mKOhOqhQ6DRs3_FHxE_CcA',
  firebaseConfig: {
    apiKey: "AIzaSyD43_ePa80cyQdknw9K_lMgkNQiyD0cVNE",
    authDomain: "nodeapp-7930e.firebaseapp.com",
    databaseURL: "https://nodeapp-7930e.firebaseio.com",
    projectId: "nodeapp-7930e",
    storageBucket: "nodeapp-7930e.appspot.com",
    messagingSenderId: "660264988885",
    appId: "1:660264988885:web:be3e0ebaeb46789ec3725c",
    measurementId: "G-YED3ZQV020"
  }
};

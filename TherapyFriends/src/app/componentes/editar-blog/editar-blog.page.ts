import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NoticiaService } from '../shared/noticia.service';

@Component({
  selector: 'app-editar-blog',
  templateUrl: './editar-blog.page.html',
  styleUrls: ['./editar-blog.page.scss'],
})
export class EditarBlogPage implements OnInit {
  updateNotasForm: FormGroup;
  id: any;

  constructor(
    private aptService: NoticiaService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.id = this.actRoute.snapshot.paramMap.get('id');
    this.aptService.getNota(this.id).valueChanges().subscribe(res => {
      this.updateNotasForm.setValue(res);
    });
  }

  ngOnInit() {
    this.updateNotasForm = this.fb.group({
      nombre: [''],
      email: [''],
      telefono: [''],
      descripcion: ['']
    });
    console.log(this.updateNotasForm.value);
  }

  updateForm() {
    this.aptService.updateNota(this.id, this.updateNotasForm.value)
      .then(() => {
        this.router.navigate(['/home']);
      })
      .catch(error => console.log(error));
  }
}


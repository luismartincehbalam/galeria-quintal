import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditarBlogPage } from './editar-blog.page';

const routes: Routes = [
  {
    path: '',
    component: EditarBlogPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditarBlogPageRoutingModule {}

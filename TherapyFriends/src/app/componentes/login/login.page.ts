import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { User } from '../shared/user.class';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: User = new User();

  constructor(private authSvc: AuthService, private router: Router, public toastController: ToastController) { }

  ngOnInit() {}

  async onLogin() {
    const user = await this.authSvc.onLogin(this.user);
    if (user) {
      console.log('Successfully logged in!');
      this.router.navigateByUrl('/home');
    } else{
      const toast = await this.toastController.create({
        message: 'Las credenciales de inicio de sesión son incorrectas. Inténtalo de nuevo.',
        duration: 2000
      });
      toast.present();
    }
    }
  }

